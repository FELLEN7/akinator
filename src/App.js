import React from 'react';
import { Route } from "react-router-dom";
import HomeSreen from './components/HomeScreen/HomeScreen';
import RequestSelector from './components/RequestSelector/RequestSelector';
import RecorderContainer from './components/Recorder/RecorderContainer';
import SketchpadContainer from './components/Sketchpad/SketchpadContainer';

function App() {
  return (
    <>
      <Route exact path='/' render={() => <HomeSreen/>} />
      <Route path='/request-selector' render={() => <RequestSelector/>} />
      <Route path='/recorder' render={() => <RecorderContainer/>} />
      <Route path='/sketchpad' render={() => <SketchpadContainer/>} />
    </>
  );
}

export default App;
