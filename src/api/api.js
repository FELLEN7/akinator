import * as axios from 'axios';
import axiosCancel from 'axios-cancel';

const PROXY_URL = 'https://cors-anywhere.herokuapp.com/';
const AUDIO_API_URL = 'https://api.audd.io/';
const API_TOKEN = 'a06564a24c68b6eba07d9e3c941250c1';
const RESULT_PATTERN = 'timecode,apple_music,deezer,spotify';

axiosCancel(axios, {
  debug: false // default
});

const songInfoAPI = {
  getInfoByRecord: (recordedBlob) => {
    let bodyFormData = new FormData();
    bodyFormData.append("file", recordedBlob.blob);
    bodyFormData.set("api_token", API_TOKEN);
    bodyFormData.set("return", RESULT_PATTERN);
    return axios
      .post(`${PROXY_URL}${AUDIO_API_URL}`, bodyFormData, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      }).then(({ data }) => data)
  },
  getInfoByText: (text) => {
    let bodyFormData = new FormData();
    bodyFormData.set("q", text)
    bodyFormData.set("api_token", API_TOKEN);
    bodyFormData.set("return", "timecode,apple_music,deezer,spotify");
    return axios
      .post(`${PROXY_URL}https://api.audd.io/findLyrics/?`, bodyFormData, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      })
      .then(({ data }) => data);
  },
  getAudioUrl: (id) => {
    return axios.get(`${PROXY_URL}https://api.deezer.com/track/${id}`)
      .then(res => {
        return res.data.preview;
      })
  },
  getAudioUrlByName: (title, artist) => {
    return axios.get(`${PROXY_URL}https://api.deezer.com/search?q=title:"${title}" artist:"${artist}"`)
      .then(res => res)
  }
}

export const cancelRequests = () => {
  axios.cancelAll();
}


export default songInfoAPI;

