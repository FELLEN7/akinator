import React from 'react';
import { connect } from 'react-redux';
import Recorder from './Recorder';
import ReturnButton from '../common/ReturnButton/ReturnButton';
import HelpingBox from '../common/HelpingBox/HelpingBox';
import {
  setRecognitionStatus,
  setAudioLoadingStatus,
  getSongDataByRecordThunkCreator,
  setResponseRecognitionStatus,
  getAudioUrlByRecordThunkCreator,
  setSongInfo,
  setAudioUrl
} from '../../redux/playerReducer';
import { useEffect } from 'react';

const RecorderContainer = (props) => {

  const getInfoByRecord = (recordedBlob) => {
    props.getSongDataByRecordThunkCreator(recordedBlob);
  }

  useEffect(() => {
    if(props.songInfo) 
      props.getAudioUrlByRecordThunkCreator(props.songInfo.deezer.id);
  }, [props.songInfo])

  const getAudio = (id) => {
    props.getAudioUrlByRecordThunkCreator(id);
  }



  return (
    <>
      {
        props.responseRecognitionStatus === 'success' &&
        !props.songInfo &&
        !props.isRecognition &&
        <HelpingBox text='TRY AGAIN. PRESS ON THE CIRCLE IN CENTER'/>
      }
      <ReturnButton url='request-selector'/>
      <Recorder
        {...props}
        getInfoByRecord={getInfoByRecord}
        getAudio={getAudio}
      />
    </>
  )
}


let mapStateToProps = (state) => {
  return {
    isRecognition: state.recorderPage.isRecognition,
    isAudioLoading: state.recorderPage.isAudioLoading,
    songInfo: state.recorderPage.songInfo,
    audioURL: state.recorderPage.audioURL,
    responseRecognitionStatus: state.recorderPage.responseRecognitionStatus
  }
}

export default connect(mapStateToProps, {
  setRecognitionStatus,
  setAudioLoadingStatus,
  getSongDataByRecordThunkCreator,
  getAudioUrlByRecordThunkCreator,
  setSongInfo,
  setAudioUrl,
  setResponseRecognitionStatus
})(RecorderContainer)