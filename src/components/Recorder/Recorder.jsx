import React, { useState, useEffect } from 'react';
import { ReactMic } from 'react-mic';
import styles from './Recorder.module.css';
import record_img from '../../img/record.svg';
import Preloader from '../../components/common/Preloader/Preloader';
import SongInfo from '../common/SongInfo/SongInfo';
import { cancelRequests } from '../../api/api';

const Recorder = (props) => {

  let [permission, setPermission] = useState(false);

  useEffect(() => {
    checkPermission();
    return () => {
      setPermission(false);
      props.setSongInfo(null);
      props.setAudioLoadingStatus(false);
      props.setAudioUrl(null);
      props.setResponseRecognitionStatus(null);
      props.setRecognitionStatus(false);
      cancelRequests();
    }
  }, []);

  useEffect(() => {
    if (permission) {
      startRecording(5000);
    }
  }, [permission])

  const checkPermission = () => {
    navigator.mediaDevices.getUserMedia({ audio: true })
      .then(() => setPermission(true))
      .catch((err) => errHandler(err))
  }

  const errHandler = (err) => {
    console.log(`Something wrong: ${err}`);
  }

  const repeatRecord = () => {
    if (!props.isRecognition && !props.isAudioLoading && !props.songInfo) {
      checkPermission();
    }
  }

  const startRecording = (time) => {
    setTimeout(() => { setPermission(false) }, time);
  }

  const onStop = (recordedBlob) => {
    props.getInfoByRecord(recordedBlob);
  }

  if (props.responseRecognitionStatus !== 'success' || props.songInfo === null) {
    return (
      <div>
        {
          props.isRecognition ? <Preloader /> :
            <div className='container'>
              <div onClick={repeatRecord} className={[styles.record_box, permission && styles.pulsation].join(' ')}>
                <img src={record_img} alt='img' />
                <ReactMic
                  className={styles.mic}
                  record={permission}      /* IF TRUE -> RECORDING */
                  onStop={onStop}
                />
              </div>
            </div>
        }
      </div>
    )
  } else {
    return (
      <div>
        <SongInfo
          songData={props.songInfo}
          isAudioLoading={props.isAudioLoading}
          audioURL={props.audioURL}
        />
      </div>
    )
  }
}

export default Recorder;