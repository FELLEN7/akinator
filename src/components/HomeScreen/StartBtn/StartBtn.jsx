import React from 'react';
import {Link} from 'react-router-dom';
import styles from './StartBtn.module.css';

const StartBtn = (props) => {
  return (
    <Link to={props.link}>
      <button className={[styles.btn, styles.btn_start, 'test'].join(' ')}>
        <img src={props.img} alt='img'/>
      </button>
    </Link>
  )
}

export default StartBtn;