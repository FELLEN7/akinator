import React from 'react'
import StartBtn from './StartBtn/StartBtn';
import start_img from '../../img/play.svg';

const HomeScreen = (props) => {

  return (
    <div className='container'>
      <StartBtn img={start_img} link='/request-selector'/>
    </div>
  )
}

export default HomeScreen;