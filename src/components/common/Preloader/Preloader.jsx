import React from 'react';
import styles from './Preloader.module.css';
import preloader_img from '../../../assets/preloader.svg';

export default (props) => {
  return (
    <div className={styles.preloader_box}>
      <img src={preloader_img} alt='Loading...'/>
    </div>
  )
}