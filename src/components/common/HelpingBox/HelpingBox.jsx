import React from 'react';
import styles from './Helping.module.css';
import * as d3 from 'd3';
import { useEffect } from 'react';

const HelpingBox = (props) => {

  useEffect(() => {
    d3.select('.hidden_element')
      .transition().duration(1000).ease(d3.easeLinear)
      .style('opacity', 1);
    setTimeout(() => {
      d3.select('.hidden_element')
        .transition().duration(2000).ease(d3.easeLinear)
        .style('opacity', 0);
    }, 3000)
  })

  return (
    <div className={[styles.container, 'hidden_element'].join(' ')}>
      <span>{props.text}</span>
    </div>
  )
}

export default HelpingBox;