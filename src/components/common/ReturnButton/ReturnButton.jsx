import React from 'react';
import return_img from '../../../img/return.svg';
import styles from './ReturnButton.module.css';
import { Link } from 'react-router-dom';

const ReturnButton = (props) => {
  return (
    <Link to={`/${props.url}`}>
      <button className={styles.return_btn}>
        <img src={return_img} alt='return' />
      </button>
    </Link>
  )
}

export default ReturnButton;