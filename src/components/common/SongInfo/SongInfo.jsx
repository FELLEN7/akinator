import React, { useState } from 'react';
import ReactPlayer from 'react-player'
import styles from './SongInfo.module.css';

const SongInfo = (props) => {

  let [isPlaying, setPlaying] = useState(false);

  const onPlay = () => {
    setPlaying(!isPlaying);
  }

  return (
    <div className='container'>
      {props.songData &&
        <div className={styles.info_box}>
          <h1>{props.songData.title}</h1>
          <h2>{props.songData.artist}</h2>
          {
            props.isAudioLoading && props.audioURL &&
            <div>
              <button className={styles.play_btn} onClick={onPlay}>{isPlaying ? `Pause` : `Play`}</button>
              <ReactPlayer
                url={props.audioURL}
                playing={isPlaying}
                loop={true}
                height='0px'
                width='0px'
              />
            </div>
          }
        </div>
      }
    </div >
  )
}

export default SongInfo;