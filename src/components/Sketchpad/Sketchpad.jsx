import React from 'react'
import { useState } from 'react';
import styles from './Sketchpad.module.css';
import Preloader from '../common/Preloader/Preloader';
import SongInfo from '../common/SongInfo/SongInfo';
import { cancelRequests } from '../../api/api';
import { useEffect } from 'react';

const Sketchpad = (props) => {

  let [text, setText] = useState('');

  useEffect(() => {
    return () => {
      props.setSongInfo(null);
      props.setAudioLoadingStatus(false);
      props.setAudioUrl(null);
      props.setResponseRecognitionStatus(null);
      props.setRecognitionStatus(false);
      cancelRequests();
    }
  }, [])

  const onChangeHandler = (e) => {
    setText(e.currentTarget.value)
  }

  const getInfoByText = () => {
    props.getInfoByText(text);
  }
  return (
    <div className='container'>
      {
        (props.responseRecognitionStatus !== 'success' || props.songInfo === null)
          ?
          <div>
            {
              props.isRecognition ? <Preloader /> :
                <div className={styles.input_box}>
                  <input
                    onChange={onChangeHandler}
                    value={text}
                    type='text'
                    placeholder='Введите текст песни' />
                  <button onClick={getInfoByText}>Search</button>
                </div>
            }
          </div>
          :
          <SongInfo
            songData={props.songInfo[0]}
            isAudioLoading={props.isAudioLoading}
            audioURL={props.audioURL}
          />
      }
    </div>
  )
}

export default Sketchpad;