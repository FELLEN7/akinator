import React from 'react';
import { connect } from 'react-redux';
import Sketchpad from './Sketchpad';
import ReturnButton from '../common/ReturnButton/ReturnButton';
import HelpingBox from '../common/HelpingBox/HelpingBox';
import {
  setRecognitionStatus,
  setAudioLoadingStatus,
  getSongDataByTextThunkCreator,
  setResponseRecognitionStatus,
  getAudioUrlByNameThunkCreator,
  setSongInfo,
  setAudioUrl
} from '../../redux/playerReducer';
import { useEffect } from 'react';

const SketchpadContainer = (props) => {

  const getInfoByText = (text) => {
    props.getSongDataByTextThunkCreator(text);
  }

  useEffect(() => {
    if(props.songInfo) {
      let {title, artist} = props.songInfo[0];
      props.getAudioUrlByNameThunkCreator(title, artist);
    }
  }, [props.songInfo])


  const getAudio = (title, artist) => {
    props.getAudioUrlByNameThunkCreator(title, artist);
  }

  return (
    <>
      {
        props.responseRecognitionStatus === 'success' &&
        !props.songInfo &&
        !props.isRecognition &&
        <HelpingBox text='TRY AGAIN'/>
      }
      <ReturnButton url='request-selector' /> 
      <Sketchpad
        {...props}
        getInfoByText={getInfoByText}
        getAudio={getAudio}
      />
    </>
  )
}

let mapStateToProps = (state) => {
  return {
    isRecognition: state.recorderPage.isRecognition,
    isAudioLoading: state.recorderPage.isAudioLoading,
    songInfo: state.recorderPage.songInfo,
    audioURL: state.recorderPage.audioURL,
    responseRecognitionStatus: state.recorderPage.responseRecognitionStatus,
    helpingPhrase: state.recorderPage.helpingPhrase
  }
}

export default connect(mapStateToProps, {
  setRecognitionStatus,
  setAudioLoadingStatus,
  getSongDataByTextThunkCreator,
  getAudioUrlByNameThunkCreator,
  setSongInfo,
  setAudioUrl,
  setResponseRecognitionStatus
})(SketchpadContainer);