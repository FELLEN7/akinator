import React from 'react';
import {Link} from 'react-router-dom';
import styles from './RequestTypeBtn.module.css';

const RequestTypeBtn = (props) => {
  return (
    <Link to={props.link}>
      <button className={styles.btn}>
        <img src={props.img} alt='button'/>
      </button>
    </Link>
  )
}

export default RequestTypeBtn;