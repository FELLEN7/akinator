import React from 'react'
import RequestTypeBtn from './RequestTypeBtn/RequestTypeBtn';
import styles from './RequestSelector.module.css';
import keyboard_img from '../../img/keyboard.svg';
import microphone_img from '../../img/microphone.svg';
import ReturnButton from '../common/ReturnButton/ReturnButton';

const RequestSelector = (props) => {


  return (
    <div className='container'>
      <ReturnButton url=''/>
      <div className={styles.selector_box}>
        <RequestTypeBtn img={microphone_img} link={'/recorder'}/>
        <RequestTypeBtn img={keyboard_img} link='/sketchpad'/>
      </div>
    </div>
  )
}

export default RequestSelector;