import songInfoAPI from '../api/api';
 
const SET_RECOGNITION_STATUS = 'SET_RECOGNITION_STATUS';
const SET_AUDIO_LOADING_STATUS = 'SET_AUDIO_LOADING_STATUS';
const SET_RESPONSE_RECOGNITION_STATUS = 'SET_RESPONSE_RECOGNITION_STATUS';
const SET_SONG_INFO = 'SET_SONG_INFO';
const SET_AUDIO_URL = 'SET_AUDIO_URL';
const SET_HELPING_PHRASE = 'SET_HELPING_PHRASE';

let initialState = {
  isRecognition: false,
  isAudioLoading: false,
  audioURL: null,
  responseRecognitionStatus: false,
  songInfo: null,
  helpingPhrase: ''
};

const playerReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_RECOGNITION_STATUS: {
      return {
        ...state,
        isRecognition: action.recognitionStatus
      }
    }
    case SET_AUDIO_LOADING_STATUS: {
      return {
        ...state,
        isAudioLoading: action.audioLoadingStatus
      }
    }
    case SET_RESPONSE_RECOGNITION_STATUS: {
      return {
        ...state,
        responseRecognitionStatus: action.status
      }
    }
    case SET_SONG_INFO: {
      return {
        ...state,
        songInfo: action.songInfo
      }
    }
    case SET_AUDIO_URL: {
      return {
        ...state,
        audioURL: action.url
      }
    }
    case SET_HELPING_PHRASE: {
      return {
        ...state,
        helpingPhrase: action.phrase
      }
    }
    default: 
      return state;
  }
}

export const setRecognitionStatus = (recognitionStatus) => ({ type: SET_RECOGNITION_STATUS, recognitionStatus });
export const setAudioLoadingStatus = (audioLoadingStatus) => ({ type: SET_AUDIO_LOADING_STATUS, audioLoadingStatus });
export const setResponseRecognitionStatus = (status) => ({ type: SET_RESPONSE_RECOGNITION_STATUS, status });
export const setSongInfo = (songInfo) => ({ type: SET_SONG_INFO, songInfo});
export const setAudioUrl = (url) => ({ type: SET_AUDIO_URL, url });
export const setHelpingPhrase = (phrase) => ({ type: SET_HELPING_PHRASE, phrase });

export const getAudioUrlByRecordThunkCreator = (id) => {
  return (dispatch) => {
    dispatch(setAudioLoadingStatus(false));
    songInfoAPI.getAudioUrl(id).then(url => {
      dispatch(setAudioUrl(url));
      dispatch(setAudioLoadingStatus(true));
    })
  }
}

export const getAudioUrlByNameThunkCreator = (title, artist) => {
  return (dispatch) => {
    dispatch(setAudioLoadingStatus(false));
    songInfoAPI.getAudioUrlByName(title, artist).then(res => {
        res.data.data.forEach(element => {
          let curr_title = element.title.toLowerCase();
          if(title.toLowerCase() === curr_title){
            dispatch(setAudioUrl(element.preview));
            dispatch(setAudioLoadingStatus(true));
          }
        });
    })
  }
}

export const getSongDataByRecordThunkCreator = (blob) => {
  return (dispatch) => {
    dispatch(setRecognitionStatus(true));
    songInfoAPI.getInfoByRecord(blob).then(data => {
        dispatch(setSongInfo(data.result));
        dispatch(setResponseRecognitionStatus(data.status));
        dispatch(setRecognitionStatus(false));
    })
  }
}

export const getSongDataByTextThunkCreator = (text) => {
  return (dispatch) => {
    dispatch(setRecognitionStatus(true));
    songInfoAPI.getInfoByText(text).then(data => {
        if(data.status !== 'error' && data.result.length !== 0){
          dispatch(setSongInfo(data.result));
        } else {
          dispatch(setSongInfo(null));
          dispatch(setHelpingPhrase('Try again'));
          // dispatch(setResponseRecognitionStatus('help'));
        }
        dispatch(setResponseRecognitionStatus(data.status));
        dispatch(setRecognitionStatus(false));
    })
  }
}


export default playerReducer;