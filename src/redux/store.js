import { combineReducers, createStore, applyMiddleware } from 'redux';
import playerReducer from './playerReducer';
import thunkMiddleware from 'redux-thunk';

let reducers = combineReducers({
  recorderPage: playerReducer,
});

let store = createStore(reducers, applyMiddleware(thunkMiddleware));

export default store;